defmodule ExBankingTest do
  use ExUnit.Case
  doctest ExBanking

  describe "create_user/1" do
    test "returns ok when user is created with success" do
      assert :ok = ExBanking.create_user("andre0")
    end

    test "returns error when arguments is invalid" do
      assert {:error, :wrong_arguments} = ExBanking.create_user(1)
    end

    test "returns error when user already exists" do
      ExBanking.create_user("andre")
      assert {:error, :user_already_exists} = ExBanking.create_user("andre")
    end
  end

  describe "deposit/3" do
    test "returns ok with updated amount" do
      user = "Jose"
      assert :ok = ExBanking.create_user(user)
      assert {:ok, 100} = ExBanking.deposit(user, 100, "BRL")
      assert {:ok, 110} = ExBanking.deposit(user, 10, "BRL")
      assert {:ok, 120} = ExBanking.deposit(user, 10, "BRL")
      assert {:ok, 100} = ExBanking.deposit(user, 100, "EUR")
      assert {:ok, 110} = ExBanking.deposit(user, 10, "EUR")
      assert {:ok, 120} = ExBanking.deposit(user, 10, "EUR")
    end

    test "returns error when arguments is invalid" do
      user = "Chris"
      assert :ok = ExBanking.create_user(user)
      assert {:error, :wrong_arguments} = ExBanking.deposit(user, "100", "BRL")
    end

    test "returns error when user does not exists" do
      assert {:error, :user_does_not_exist} = ExBanking.deposit("user", 100, "BRL")
    end

    test "returns error when user has to many requests"
  end

  describe "withdraw/3" do
    test "returns ok with updated amount" do
      user = "Jose1"
      assert :ok = ExBanking.create_user(user)
      assert {:ok, 100} = ExBanking.deposit(user, 100, "BRL")
      assert {:ok, 90} = ExBanking.withdraw(user, 10, "BRL")
    end

    test "returns error when funds is insuficient" do
      user = "Chris1"
      assert :ok = ExBanking.create_user(user)
      assert {:ok, 100.99} = ExBanking.deposit(user, 100.99, "BRL")
      assert {:error, :not_enough_money} = ExBanking.withdraw(user, 10, "USD")
      assert {:error, :not_enough_money} = ExBanking.withdraw(user, 200, "BRL")
    end

    test "returns error when arguments is invalid" do
      user = "Chris2"
      assert :ok = ExBanking.create_user(user)
      assert {:error, :wrong_arguments} = ExBanking.withdraw(user, "100", "BRL")
    end

    test "returns error when user does not exists" do
      assert {:error, :user_does_not_exist} = ExBanking.withdraw("user", 100, "BRL")
    end

    test "returns error when user has to many requests"
  end

  describe "get_balance/2" do
    test "returns ok with balance" do
      user = "Dudu"
      assert :ok = ExBanking.create_user(user)
      assert {:ok, 100} = ExBanking.deposit(user, 100, "BRL")
      assert {:ok, 100} = ExBanking.get_balance(user, "BRL")
      assert {:ok, 0} = ExBanking.get_balance(user, "USD")
    end

    test "returns error when arguments is invalid" do
      user = "Chris3"
      assert :ok = ExBanking.create_user(user)
      assert {:error, :wrong_arguments} = ExBanking.get_balance(user, nil)
    end

    test "return error when user does not exists" do
      assert {:error, :user_does_not_exist} = ExBanking.get_balance("user", "USD")
    end

    test "returns error when user has to many returns"
  end

  describe "send/4" do
    test "returns ok with balance" do
      from_user = "Dudu1"
      to_user = "Edu"
      assert :ok = ExBanking.create_user(from_user)
      assert :ok = ExBanking.create_user(to_user)
      assert {:ok, 100} = ExBanking.deposit(from_user, 100, "BRL")
      assert {:ok, 90, 10} = ExBanking.send(from_user, to_user, 10, "BRL")
      assert {:ok, 80, 20} = ExBanking.send(from_user, to_user, 10, "BRL")
      assert {:error, :not_enough_money} = ExBanking.send(from_user, to_user, 100, "USD")
      assert {:error, :not_enough_money} = ExBanking.send(from_user, to_user, 100, "AUD")
    end

    test "returns error when arguments is invalid" do
      assert {:error, :wrong_arguments} = ExBanking.send("user", nil, nil, nil)
      assert {:error, :wrong_arguments} = ExBanking.send("user", nil, "1234", 000)
      assert {:error, :wrong_arguments} = ExBanking.send("user", 123, 123, "123")
      assert {:error, :wrong_arguments} = ExBanking.send("user", "user2", 123, 1234)
      assert {:error, :wrong_arguments} = ExBanking.send("user", nil, 000, "123")
    end

    test "return error when sender user does not exists" do
      user = "Dudu2"
      assert :ok = ExBanking.create_user(user)
      assert {:error, :sender_does_not_exist} = ExBanking.send("fake_user", user, 1, "USD")
    end

    test "return error when receiver user does not exists" do
      user = "Dudu3"
      assert :ok = ExBanking.create_user(user)
      assert {:error, :receiver_does_not_exist} = ExBanking.send(user, "fake_user", 1, "USD")
    end

    test "returns error when user has to many returns"
  end
end
