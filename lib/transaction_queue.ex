defmodule ExBanking.TransactionQueue do
  use GenServer

  defmodule Transaction do
    defstruct [:type, :params]
    @type t :: %__MODULE__{type: String.t(), params: map()}
  end

  defmodule Transactions do
    defstruct transactions: []
    @type t :: %__MODULE__{transactions: [%Transaction{}]}
  end

  def start_link(user) do
    GenServer.start_link(__MODULE__, %Transactions{}, name: via(user))
  end

  def get_transactions(user) do
    user
    |> get_user_pid()
    |> GenServer.call(:state)
  end

  def queue(user), do: via(user)

  def add_transaction(user, type, params) do
    user
    |> get_user_pid()
    |> GenServer.call({:add_transaction, type, params})
  end

  def remove_transaction(user) do
    user
    |> get_user_pid()
    |> GenServer.call(:remove_transaction)
  end

  defp get_user_pid(user) do
    user
    |> via()
  end

  @impl true
  def init(state) do
    {:ok, state}
  end

  @impl true
  def handle_call(:state, _from, state) do
    {:reply, state, state}
  end

  @impl true
  def handle_call({:add_transaction, type, params}, _from, state) do
    if can_process_transaction?(state.transactions) do
      transactions = state.transactions ++ [%Transaction{type: type, params: params}]
      new_state = %{transactions: transactions}
      {:reply, :ok, new_state}
    else
      {:reply, :too_many_requests_to_user, state}
    end
  end

  @impl true
  def handle_call(:remove_transaction, _from, state) do
    [_ | transactions] = state.transactions
    new_state = %{transactions: transactions}
    {:reply, :ok, new_state}
  end

  defp can_process_transaction?(transactions) do
    Enum.count(transactions) < 10
  end

  defp via(user) do
    {:via, Registry, {TransactionQueueRegistry, user}}
  end
end
