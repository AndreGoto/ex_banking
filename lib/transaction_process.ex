defmodule ExBanking.TransactionProcess do
  alias ExBanking.{TransactionQueue, User}

  def call(user) do
    %{transactions: transactions} = TransactionQueue.get_transactions(user)

    [transaction | _] = transactions
    process(user, transaction)
  end

  def process(user, %{type: "deposit", params: params}) do
    response = User.update_currency(user, params)
    TransactionQueue.remove_transaction(user)
    response
  end

  def process(user, %{type: "withdraw", params: params}) do
    response = User.withdraw_currency(user, params)
    TransactionQueue.remove_transaction(user)
    response
  end

  def process(user, %{type: "get_balance", params: params}) do
    response = User.get_balance(user, params)
    TransactionQueue.remove_transaction(user)
    response
  end
end
