defmodule ExBanking.User do
  use GenServer
  require Logger

  defmodule Currency do
    defstruct [:currency, :amount]
  end

  defmodule Wallet do
    defstruct currencies: []
  end

  def start_link(user) do
    GenServer.start_link(__MODULE__, %Wallet{}, name: via(user))
  end

  def get_user(user) do
    Registry.lookup(UserRegistry, user)
  end

  def update_currency(user, currency) do
    user
    |> via()
    |> GenServer.call({:deposit, currency})
  end

  def withdraw_currency(user, currency) do
    user
    |> via()
    |> GenServer.call({:withdraw, currency})
  end

  def get_balance(user, currency) do
    user
    |> via()
    |> GenServer.call({:get_balance, currency})
  end

  @impl true
  def init(state) do
    {:ok, state}
  end

  @impl true
  def handle_call({:deposit, currency}, _from, state) do
    currencies =
      case Enum.filter(state.currencies, &(&1.currency == currency.currency)) do
        [] ->
          state.currencies ++ [currency]

        _ ->
          Enum.map(state.currencies, fn c ->
            case c.currency == currency.currency do
              true ->
                Map.update!(c, :amount, &(&1 + currency.amount))

              _ ->
                c
            end
          end)
      end

    new_state = %{state | currencies: currencies}

    currenct_currency = Enum.find(currencies, &(&1.currency == currency.currency))
    response = {:ok, currenct_currency.amount}
    {:reply, response, new_state}
  end

  @impl true
  def handle_call({:withdraw, currency}, _from, state) do
    case Enum.find(
           state.currencies,
           &(&1.currency == currency.currency && &1.amount >= currency.amount)
         ) do
      nil ->
        {:reply, {:error, :not_enough_money}, state}

      _ ->
        currencies =
          Enum.map(state.currencies, fn cur ->
            case cur.currency == currency.currency do
              true ->
                Map.update!(cur, :amount, &(&1 - currency.amount))

              _ ->
                cur
            end
          end)

        new_state = %{state | currencies: currencies}

        response =
          case Enum.find(new_state.currencies, &(&1.currency == currency.currency)) do
            nil ->
              {:error, :not_enough_money}

            currenct_currency ->
              {:ok, currenct_currency.amount}
          end

        {:reply, response, new_state}
    end
  end

  @impl true
  def handle_call({:get_balance, currency}, _from, state) do
    response =
      case Enum.find(state.currencies, &(&1.currency == currency.currency)) do
        nil ->
          {:ok, 0}

        currenct_currency ->
          {:ok, currenct_currency.amount}
      end

    {:reply, response, state}
  end

  defp via(user) do
    {:via, Registry, {UserRegistry, user}}
  end
end
