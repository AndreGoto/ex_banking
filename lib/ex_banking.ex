defmodule ExBanking do
  @moduledoc """
  Documentation for `ExBanking`.
  """
  alias ExBanking.{TransactionProcess, TransactionQueue, User}

  @spec create_user(user :: String.t()) :: :ok | {:error, :wrong_arguments | :user_already_exists}
  def create_user(user) when is_binary(user) do
    case ExBanking.UserDynamicSupervisor.start_worker(user) do
      :ok -> :ok
      {:error, _} -> {:error, :user_already_exists}
    end
  end

  def create_user(_), do: {:error, :wrong_arguments}

  @spec deposit(user :: String.t(), amount :: number, currency :: String.t()) ::
          {:ok, new_balance :: number}
          | {:error, :wrong_arguments | :user_does_not_exist | :too_many_requests_to_user}
  def deposit(user, amount, currency)
      when is_binary(user) and is_number(amount) and is_binary(currency) and amount > 0 do
    do_deposit(user, amount, currency)
  end

  def deposit(_, _, _), do: {:error, :wrong_arguments}

  @spec withdraw(user :: String.t(), amount :: number, currency :: String.t()) ::
          {:ok, new_balance :: number}
          | {:error,
             :wrong_arguments
             | :user_does_not_exist
             | :not_enough_money
             | :too_many_requests_to_user}
  def withdraw(user, amount, currency)
      when is_binary(user) and is_number(amount) and is_binary(currency) and amount > 0 do
    do_withdraw(user, amount, currency)
  end

  def withdraw(_, _, _), do: {:error, :wrong_arguments}

  @spec get_balance(user :: String.t(), currency :: String.t()) ::
          {:ok, balance :: number}
          | {:error, :wrong_arguments | :user_does_not_exist | :too_many_requests_to_user}
  def get_balance(user, currency) when is_binary(user) and is_binary(currency) do
    do_get_balance(user, currency)
  end

  def get_balance(_, _), do: {:error, :wrong_arguments}

  @spec send(
          from_user :: String.t(),
          to_user :: String.t(),
          amount :: number,
          currency :: String.t()
        ) ::
          {:ok, from_user_balance :: number, to_user_balance :: number}
          | {:error,
             :wrong_arguments
             | :not_enough_money
             | :sender_does_not_exist
             | :receiver_does_not_exist
             | :too_many_requests_to_sender
             | :too_many_requests_to_receiver}
  def send(from_user, to_user, amount, currency)
      when is_binary(from_user) and is_binary(to_user) and is_number(amount) and
             is_binary(currency) and amount > 0 do
    do_send(from_user, to_user, amount, currency)
  end

  def send(_, _, _, _), do: {:error, :wrong_arguments}

  defp do_deposit(user, amount, currency) do
    with {:ok, _} <- get_user(user) do
      process_transaction(user, "deposit", %{amount: amount, currency: currency})
    else
      {:error, :user_does_not_exist} ->
        {:error, :user_does_not_exist}

      {_, _} ->
        {:error, :unexpected}
    end
  end

  defp do_withdraw(user, amount, currency) do
    with {:ok, _} <- get_user(user) do
      process_transaction(user, "withdraw", %{amount: amount, currency: currency})
    end
  end

  defp do_get_balance(user, currency) do
    with {:ok, _} <- get_user(user) do
      process_transaction(user, "get_balance", %{currency: currency})
    end
  end

  defp do_send(from_user, to_user, amount, currency) do
    case get_user(from_user) do
      {:error, _} ->
        {:error, :sender_does_not_exist}

      {:ok, _} ->
        case get_user(to_user) do
          {:error, _} ->
            {:error, :receiver_does_not_exist}

          {:ok, _} ->
            with {:ok, _} <- ExBanking.User.get_balance(from_user, %{currency: currency}),
                 {:ok, from_user_balance} <-
                   process_transaction(from_user, "withdraw", %{
                     amount: amount,
                     currency: currency
                   }),
                 {:ok, to_user_balance} <-
                   process_transaction(to_user, "deposit", %{amount: amount, currency: currency}) do
              {:ok, from_user_balance, to_user_balance}
            else
              {:error, _} ->
                {:error, :not_enough_money}
            end
        end
    end
  end

  defp process_transaction(user, type, params) do
    TransactionQueue.add_transaction(user, type, params)
    TransactionProcess.call(user)
  end

  defp start_user_queue(user) do
    case Process.whereis(TransactionQueue.queue(user)) do
      nil ->
        TransactionQueue.start_link(user)

      pid ->
        {:ok, pid}
    end
  end

  defp get_user(user) do
    case User.get_user(user) do
      [] ->
        {:error, :user_does_not_exist}

      pid ->
        {:ok, pid}
    end
  end
end
