defmodule ExBanking.UserSupervisor do
  use DynamicSupervisor

  alias ExBanking.User

  def start_link(_arg) do
    DynamicSupervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def start_user(user) do
    user_server(user)
  end

  def init(:ok), do: DynamicSupervisor.init(strategy: :one_for_one)

  defp user_server(user) do
    spec = %{id: User, start: {User, :start_link, [user]}}
    DynamicSupervisor.start_child(__MODULE__, spec)
  end
end
