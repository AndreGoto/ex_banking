defmodule ExBanking.UserDynamicSupervisor do
  use DynamicSupervisor
  alias ExBanking.User

  def start_link(_) do
    DynamicSupervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  def start_worker(user) do
    case already_exists?(user) do
      true ->
        {:error, :user_already_exists}

      false ->
        with {:ok, _pid} <- start_child_wallet(user),
             {:ok, _pid} <- start_transaction_queue(user) do
          :ok
        end
    end
  end

  defp already_exists?(user) do
    case Registry.lookup(UserRegistry, user) do
      [{_pid, _}] -> true
      [] -> false
    end
  end

  defp start_child_wallet(user) do
    DynamicSupervisor.start_child(__MODULE__, {ExBanking.User, user})
  end

  defp start_transaction_queue(user) do
    DynamicSupervisor.start_child(__MODULE__, {ExBanking.TransactionQueue, user})
  end

  def init(_args), do: DynamicSupervisor.init(strategy: :one_for_one)
end
