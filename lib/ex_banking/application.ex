defmodule ExBanking.Application do
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      {Registry, keys: :unique, name: UserRegistry},
      {Registry, keys: :unique, name: TransactionQueueRegistry},
      ExBanking.UserDynamicSupervisor
    ]

    opts = [strategy: :one_for_one]

    Supervisor.start_link(children, opts)
  end
end
